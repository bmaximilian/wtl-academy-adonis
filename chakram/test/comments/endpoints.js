/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

module.exports = {
    list: '/posts/{postId}/comments',
    add: '/posts/{postId}/comments',
    get: '/posts/{postId}/comments/{id}',
    edit: '/posts/{postId}/comments/{id}',
    destroy: '/posts/{postId}/comments/{id}',
};
