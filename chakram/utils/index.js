/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const getApiUrl = require('./getApiUrl');
const checkIfUser = require('./checkIfUser');
const checkIfPost = require('./checkIfPost');
const checkIfComment = require('./checkIfComment');
const checkForSuccessfulJsonResponse = require('./checkForSuccessfulJsonResponse');
const checkIfResponse = require('./checkIfResponse');

module.exports = {
    getApiUrl,
    checkIfUser,
    checkIfPost,
    checkIfComment,
    checkForSuccessfulJsonResponse,
    checkIfResponse,
};
